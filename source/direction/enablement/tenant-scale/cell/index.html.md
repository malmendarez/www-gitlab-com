---
layout: markdown_page
title: "Category Direction - Cell"
description: ""
canonical_path: "/direction/enablement/tenant-scale/cell/"
---

- TOC
{:toc}

## Cell

| | |
| --- | --- |
| Stage | [Data Stores](/direction/enablement/) |
| Maturity | [Not Applicable](/direction/maturity/) |
| Content Last Reviewed | `2023-05-10` |

## Introduction and how you can help

Thanks for visiting this category direction page on Cells at GitLab. The Cell category is part of the [Tenant Scale group](https://about.gitlab.com/handbook/product/categories/#tenant-scale-group) within the [Enablement](https://about.gitlab.com/direction/enablement/) section and is maintained by [Christina Lohr](https://about.gitlab.com/company/team/#lohrc). 

This vision and direction is constantly evolving and everyone can contribute:
* Please comment in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=group%3A%3Atenant%20scale&label_name%5B%5D=Category%3ACell&first_page_size=100) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?state=opened&page=1&sort=start_date_desc&label_name[]=group::tenant+scale&label_name[]=Category:Cell). Sharing your feedback directly on GitLab.com or submitting a merge request to this page are the best ways to contribute to our strategy.
* Please share feedback directly via [email](https://gitlab.com/lohrc), or [schedule a video call](https://calendly.com/christinalohr/30min).
* Please open an issue using the ~"Category:Cell" label, or reach out to us internally in the #g_tenant-scale Slack channel.